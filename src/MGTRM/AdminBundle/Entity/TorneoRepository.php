<?php
namespace MGTRM\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TorneoRepository extends EntityRepository{

	public function getActiveTournaments(){
		return $this->createQueryBuilder('t')
			->where('t.status = 1')
			->getQuery()
			->getResult();
	}

	public function getPendingTournaments(){
		return $this->createQueryBuilder('t')
			->where('t.status = 2')
			->getQuery()
			->getResult();
	}

	public function getClosedTournaments(){
		return $this->createQueryBuilder('t')
			->where('t.status = 3')
			->getQuery()
			->getResult();
	}
}
