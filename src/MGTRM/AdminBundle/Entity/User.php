<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 04/03/14
 * Time: 17:03
 */

namespace MGTRM\AdminBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table{name="Users"}
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields="name",message="El usuario especificado ya existe")
 * @UniqueEntity(fields="mail",message="El correo electrónico especificado ya existe")
 * @UniqueEntity(fields="psn",message="El id de Playstation Network especificado ya existe")
 */
class User implements UserInterface{

	public function __construct(){
		$this->resultados=new ArrayCollection();
		$this->inscripciones=new ArrayCollection();
	}

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $name;


	/**
	 * @ORM\Column(type="string", length=255)
	 */
	protected $password;

	/**
	 * @ORM\Column(type="string", length=60)
	 */

	protected $roles;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 * @Assert\Email()
	 */

	protected $mail;

	/**
	 * @ORM\Column(type="string", length=255, unique=true)
	 */

	protected $psn;


	/**
	 * @ORM\OneToMany(targetEntity="Results", mappedBy="user")
	 */
	protected $resultados;

	/**
	 * @ORM\ManyToMany(targetEntity="Torneo", inversedBy="inscritos")
	 * @ORM\JoinTable(name="inscripciones")
	 */
	protected $inscripciones;

	/**
	 * @ORM\Column(type="datetime")
	 */

	protected $register_date;

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $mail
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
	}

	/**
	 * @return mixed
	 */
	public function getMail()
	{
		return $this->mail;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $psn
	 */
	public function setPsn($psn)
	{
		$this->psn = $psn;
	}

	/**
	 * @return mixed
	 */
	public function getPsn()
	{
		return $this->psn;
	}

	/**
	 * @param mixed $register_date
	 */
	public function setRegisterDate($register_date)
	{
		$this->register_date = $register_date;
	}

	/**
	 * @return mixed
	 */
	public function getRegisterDate()
	{
		return $this->register_date;
	}

	/**
	 * @param mixed $role
	 */
	public function setRole($role)
	{
		$this->role[]=$role;
	}

	/**
	 * @return mixed
	 */
	public function getRole()
	{
		return $this->role;
	}





    /**
     * Add resultados
     *
     * @param \MGTRM\AdminBundle\Entity\Results $resultados
     * @return User
     */
    public function addResultado(\MGTRM\AdminBundle\Entity\Results $resultados)
    {
        $this->resultados[] = $resultados;
    
        return $this;
    }

    /**
     * Remove resultados
     *
     * @param \MGTRM\AdminBundle\Entity\Results $resultados
     */
    public function removeResultado(\MGTRM\AdminBundle\Entity\Results $resultados)
    {
        $this->resultados->removeElement($resultados);
    }

    /**
     * Get resultados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResultados()
    {
        return $this->resultados;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

	/**
	 * @ORM\PrePersist
	 */
	public function setDate(){
		if($this->register_date==null)
		{
			$this->register_date=new \DateTime();
		}
	}

    /**
     * Add inscripciones
     *
     * @param \MGTRM\AdminBundle\Entity\Torneo $inscripciones
     * @return User
     */
    public function addInscripcion(\MGTRM\AdminBundle\Entity\Torneo $inscripciones)
    {
        $this->inscripciones[] = $inscripciones;
    
        return $this;
    }

    /**
     * Remove inscripciones
     *
     * @param \MGTRM\AdminBundle\Entity\Torneo $inscripciones
     */
    public function removeInscripcion(\MGTRM\AdminBundle\Entity\Torneo $inscripciones)
    {
        $this->inscripciones->removeElement($inscripciones);
    }

    /**
     * Get inscripciones
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInscripciones()
    {
        return $this->inscripciones;
    }

	/**
	 * Returns the roles granted to the user.
	 *
	 * <code>
	 * public function getRoles()
	 * {
	 *     return array('ROLE_USER');
	 * }
	 * </code>
	 *
	 * Alternatively, the roles might be stored on a ``roles`` property,
	 * and populated in any number of different ways when the user object
	 * is created.
	 *
	 * @return Role[] The user roles
	 */
	public function getRoles()
	{
		return array($this->roles);
	}

	/**
	 * Returns the salt that was originally used to encode the password.
	 *
	 * This can return null if the password was not encoded using a salt.
	 *
	 * @return string|null The salt
	 */
	public function getSalt()
	{
		return null;
	}

	/**
	 * Returns the username used to authenticate the user.
	 *
	 * @return string The username
	 */
	public function getUsername()
	{
		return $this->getName();
	}

	/**
	 * Removes sensitive data from the user.
	 *
	 * This is important if, at any given point, sensitive information like
	 * the plain-text password is stored on this object.
	 */
	public function eraseCredentials()
	{

	}

    /**
     * Set roles
     *
     * @param string $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    
        return $this;
    }
}