<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 04/03/14
 * Time: 17:21
 */

namespace MGTRM\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="MGTRM\AdminBundle\Entity\TorneoRepository")
 * @ORM\Table{name="Torneo"}
 * @Assert\Callback(methods={"isFutureDate"})
 */
class Torneo {

	const STATUS_ACTIVE=1;
	const STATUS_WAITING=2;
	const STATUS_APPROVED=3;

	public function __construct(){
		$this->resultados=new ArrayCollection();
		$this->inscritos=new ArrayCollection();
		$this->status=self::STATUS_ACTIVE;
	}



	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */

	protected $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="El campo 'Nombre' no puede estar vacio")
	 */

	protected $name;

	/**
	 * @ORM\Column(type="integer")
	 * @Assert\Range(min=1,max=14, maxMessage="El número de participantes máximos debe de estar entre 1 y 14", minMessage="El número de participantes máximos debe de estar entre 1 y 14")
	 */

	protected $maxParticipantes;

	/**
	 * @ORM\Column(type="datetime")
	 */

	protected $tDatetime;

	/**
	 * @ORM\Column(type="text")
	 */

	protected $description;

	/**
	 * @ORM\Column(type="integer")
	 */

	protected $status;

	/**
	 * @ORM\Column(type="string",nullable=true)
	 */

	protected $resultImage;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */

	protected $imageMime;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */

	protected $uploaderId;


	/**
	 * @ORM\OneToMany(targetEntity="Results",mappedBy="tournament")
	 */
	protected $resultados;

	/**
	 * @ORM\ManyToMany(targetEntity="User",mappedBy="inscripciones")
	 */
	protected $inscritos;

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $imageMime
	 */
	public function setImageMime($imageMime)
	{
		$this->imageMime = $imageMime;
	}

	/**
	 * @return mixed
	 */
	public function getImageMime()
	{
		return $this->imageMime;
	}

	/**
	 * @param mixed $maxParticipantes
	 */
	public function setMaxParticipantes($maxParticipantes)
	{
		$this->maxParticipantes = $maxParticipantes;
	}

	/**
	 * @return mixed
	 */
	public function getMaxParticipantes()
	{
		return $this->maxParticipantes;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $resultImage
	 */
	public function setResultImage($resultImage)
	{
		$this->resultImage = $resultImage;
	}

	/**
	 * @return mixed
	 */
	public function getResultImage()
	{
		return $this->resultImage;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $tDatetime
	 */
	public function setTDatetime($tDatetime)
	{
		$this->tDatetime = $tDatetime;
	}

	/**
	 * @return mixed
	 */
	public function getTDatetime()
	{
		return $this->tDatetime;
	}

	/**
	 * @param mixed $uploaderId
	 */
	public function setUploaderId($uploaderId)
	{
		$this->uploaderId = $uploaderId;
	}

	/**
	 * @return mixed
	 */
	public function getUploaderId()
	{
		return $this->uploaderId;
	}




    /**
     * Add resultados
     *
     * @param \MGTRM\AdminBundle\Entity\Results $resultados
     * @return Torneo
     */
    public function addResultado(\MGTRM\AdminBundle\Entity\Results $resultados)
    {
        $this->resultados[] = $resultados;
    
        return $this;
    }

    /**
     * Remove resultados
     *
     * @param \MGTRM\AdminBundle\Entity\Results $resultados
     */
    public function removeResultado(\MGTRM\AdminBundle\Entity\Results $resultados)
    {
        $this->resultados->removeElement($resultados);
    }

    /**
     * Get resultados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResultados()
    {
        return $this->resultados;
    }

    /**
     * Add inscritos
     *
     * @param \MGTRM\AdminBundle\Entity\User $inscritos
     * @return Torneo
     */
    public function addInscrito(User $inscritos)
    {
        $this->inscritos[] = $inscritos;
    
        return $this;
    }

    /**
     * Remove inscritos
     *
     * @param \MGTRM\AdminBundle\Entity\User $inscritos
     */
    public function removeInscrito(User $inscritos)
    {
        $this->inscritos->removeElement($inscritos);
    }

    /**
     * Get inscritos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInscritos()
    {
        return $this->inscritos;
    }

	public function getPartCount(){
		return $this->getInscritos()->count();
	}

	public function isFutureDate(ExecutionContextInterface $context){
		$current = new \DateTime();
		$diff=$current->diff($this->tDatetime);
		if($diff->invert==1||$diff->days==0){
			$context->addViolationAt('tDatetime','La fecha/hora del torneo debe ser por lo menos 24 horas despues de la creación de este');
		}
	}
}