<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 04/03/14
 * Time: 17:30
 */

namespace MGTRM\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="MGTRM\AdminBundle\Entity\ResultsRepository")
 * @ORM\Table{name="Results"}
 * @UniqueEntity(fields = {"tournament","user"})
 */


class Results {

	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Torneo",inversedBy="resultados")
	 */
	protected $tournament;

	/**
	 * @ORM\ManyToOne(targetEntity="User",inversedBy="resultados")
	 */
	protected $user;


	/**
	 * @ORM\Column(type="integer")
	 */
	protected $position;

	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

    /**
     * Set tournament
     *
     * @param \MGTRM\AdminBundle\Entity\Torneo $tournament
     * @return Results
     */
    public function setTournament(Torneo $tournament)
    {
        $this->tournament = $tournament;
    
        return $this;
    }

    /**
     * Get tournament
     *
     * @return \MGTRM\AdminBundle\Entity\Torneo 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set user
     *
     * @param \MGTRM\AdminBundle\Entity\User $user
     * @return Results
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \MGTRM\AdminBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}