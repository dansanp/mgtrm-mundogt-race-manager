<?php

namespace MGTRM\AdminBundle\Controller;

use MGTRM\AdminBundle\Entity\Results;
use MGTRM\AdminBundle\Form\ValidateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MGTRM\AdminBundle\Entity\Torneo;
use MGTRM\AdminBundle\Form\TorneoType;

/**
 * Torneo controller.
 *
 */
class TorneoController extends Controller
{

    /**
     * Lists all Torneo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminBundle:Torneo')->findAll();

        return $this->render('AdminBundle:Tournament:list.html.twig', array(
            'entities' => $entities,
			'action'=>'list'
        ));
    }
    /**
     * Creates a new Torneo entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Torneo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_homepage'));
        }

        return $this->render('AdminBundle:Tournament:create.html.twig', array(
			'action'=>'newTournament',
			'errors'=>$form->getErrorsAsString(),
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Torneo entity.
    *
    * @param Torneo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Torneo $entity)
    {
        $form = $this->createForm(new TorneoType(), $entity, array(
            'action' => $this->generateUrl('admin_torneo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
			'label' => 'Crear Torneo',
			'attr' => array(
				'class' => 'btn btn-primary'
			)
		));

        return $form;
    }

    /**
     * Displays a form to create a new Torneo entity.
     *
     */
    public function newAction()
    {
        $entity = new Torneo();
        $form   = $this->createCreateForm($entity);

        return $this->render('AdminBundle:Tournament:create.html.twig', array(
            'action' => 'newTournament',
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Torneo entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Torneo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Torneo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Torneo:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
		));
    }

    /**
     * Displays a form to edit an existing Torneo entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Torneo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Torneo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminBundle:Tournament:create.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
			'action'=>'edit'
        ));
    }

    /**
    * Creates a form to edit a Torneo entity.
    *
    * @param Torneo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Torneo $entity)
    {
		$tType=new TorneoType;
		$tType->setIsEditing(true);
        $form = $this->createForm($tType, $entity, array(
            'action' => $this->generateUrl('admin_torneo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        $form->add('submit', 'submit', array(
			'label' => 'Editar',
			'attr' => array(
				'class' => 'btn btn-primary'
			)
		));

        return $form;
    }
    /**
     * Edits an existing Torneo entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminBundle:Torneo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Torneo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_homepage'));
        }

        return $this->render('AdminBundle:Tournament:create.html.twig', array(
			'action' => 'edit',
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Torneo entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('AdminBundle:Torneo')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Torneo entity.');
		}

		$em->remove($entity);
		$em->flush();

        return $this->redirect($this->generateUrl('admin_torneo'));
    }

    /**
     * Creates a form to delete a Torneo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_torneo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

	public function adminAction(){
		return $this->render('AdminBundle:Tournament:list.html.twig',array(
			'action'=>'list'
		));
	}

	public function pendingAction(){
		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository('AdminBundle:Torneo')->getPendingTournaments();

		return $this->render('AdminBundle:Tournament:pending.html.twig', array(
			'entities' => $entities,
			'action'=>'pendient'
		));

	}

	public function validateAction($id){
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('AdminBundle:Torneo')->find($id);
		if(!$entity){
			return $this->redirect('admin_homepage');
		}
		$participantes=$entity->getInscritos();
		$fType=new ValidateType();
		$fType->setParticipants($participantes);
		$form = $this->createForm($fType, null, array(
			'action' => $this->generateUrl('admin_process_results', array('id' => $entity->getId())),
			'method' => 'POST',
		));

		return $this->render('AdminBundle:Tournament:validate.html.twig',array(
			'action' => 'pendient',
			'torneo' => $entity,
			'form'=>$form->createView()
		));
	}

	public function postResultsAction($id){
		$request=$this->getRequest();
		$em = $this->getDoctrine()->getManager();
		$torneo= $em->getRepository('AdminBundle:Torneo')->find($id);
		if(!$torneo){
			return $this->redirect('admin_homepage');
		}
		$fType=new ValidateType();
		$fType->setParticipants($torneo->getInscritos());
		$form = $this->createForm($fType, null, array(
			'action' => $this->generateUrl('admin_validate', array('id' => $torneo->getId())),
			'method' => 'POST',
		));
		$form->handleRequest($request);
		$nParticipantes=$torneo->getInscritos()->count();

		for($i=1;$i<=$nParticipantes;$i++){
			$userName=$form['position_'.$i]->getData();
			$user=$em->getRepository('AdminBundle:User')->findOneBy(array('name'=>$userName));
			$result=new Results();
			$result->setTournament($torneo);
			$result->setUser($user);
			$result->setPosition($i);
			$em->persist($result);
		}
		$torneo->setStatus(Torneo::STATUS_APPROVED);
		$em->flush();
		return $this->redirect('admin_pending_list');
	}
}
