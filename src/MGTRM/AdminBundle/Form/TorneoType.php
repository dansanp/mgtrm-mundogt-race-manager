<?php

namespace MGTRM\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TorneoType extends AbstractType
{
	private $isEditing=false;
	private $onlyImageUpload=false;

	public function setIsEditing($var){
		$this->isEditing=$var;
	}

	public function setOnlyImageUpload($var){
		$this->onlyImageUpload=$var;
	}

        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$class='form-control';
		if(!$this->onlyImageUpload){
			$builder
				->add('name',null,array(
					'label'=>'Nombre:',
					'attr'=>array(
						'placeholder'=>'Nombre Torneo',
						'class'=>$class
					)
				))
				->add('maxParticipantes',null,array(
			'label'=>'Participantes máximos:',
			'attr'=>array(
				'placeholder'=>'Participantes máximos',
				'class'=>$class
			)
		))
				->add('description','ckeditor',array(
					'label'=>'Descripción del Torneo:'
				))
			;

			if($this->isEditing){
				$builder            ->add('tDatetime',null,array(
					'label'=>'Fecha Torneo:'
				));
			}
			else{
				$builder            ->add('tDatetime',null,array(
					'label'=>'Fecha Torneo:',
					'data'=> new \DateTime()
				));
			}
		}else{
			$builder->add('foto','file',array(
				'label'=>'Subir foto resultados',
				'attr'=>array(
					'class'=>$class
				)
			));
		}

    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MGTRM\AdminBundle\Entity\Torneo'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mgtrm_adminbundle_torneo';
    }

}
