<?php

namespace MGTRM\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null,array(
				'label'=>'Nombre:',
				'attr'=>array(
					'placeholder'=>'Usuario',
					'class'=>'form-control'
				)
			))
            ->add('mail',null,array(
				'label'=>'Correo electrónico:',
				'attr'=>array(
					'placeholder'=>'Correo electrónico',
					'class'=>'form-control'
				)
			))
			->add('password','repeated',array(
				'label'=>false,
				'first_name'=>'password',
				'second_name'=>'confirm',
				'type'=>'password',
				'first_options'=>array(
					'label'=>'Contraseña:',
					'attr'=>array(
						'class'=>'form-control',
						'placeholder'=>'Introduzca contraseña'
					)
				),
				'second_options'=>array(
					'label'=>'Repita contraseña:',
					'attr'=>array(
						'class'=>'form-control',
						'placeholder'=>'Repita contraseña'
					)
				)
			))
            ->add('psn',null,array(
				'label'=>'PSN Id:',
				'attr'=>array(
					'placeholder'=>'PSN Id',
					'class'=>'form-control'
				)
			))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MGTRM\AdminBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mgtrm_adminbundle_user';
    }
}
