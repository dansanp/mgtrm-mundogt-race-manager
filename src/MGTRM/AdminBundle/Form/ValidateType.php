<?php

namespace MGTRM\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ValidateType extends AbstractType
{
	private $_participants;

	public function setParticipants($var){
		$arrUsers=$var->toArray();
		$arrPart=array();
		foreach($arrUsers as $elem){
			$arrPart[$elem->getName()]=$elem->getName();
		}
		$this->_participants=$arrPart;
	}
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

		$i=1;
		foreach($this->_participants as $elem){
			$builder->add('position_'.$i,'choice',array(
				'label'=>'Posicion '.$i.':',
				'choices'=>$this->_participants,
				'empty_value'=>'Seleccione un piloto:',
				'attr'=>array(
					'class'=>'form-control'
				)
			));
			$i++;
		}
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ValidateType';
    }
}
