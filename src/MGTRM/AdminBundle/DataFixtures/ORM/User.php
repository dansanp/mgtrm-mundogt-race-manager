<?php

namespace MGTRM\AdminBundle\DataFixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MGTRM\AdminBundle\Entity;

class User extends AbstractFixture implements OrderedFixtureInterface{
	public function getOrder()
	{
		return 2;
	}

	public function load(ObjectManager $manager){
		$user= new Entity\User();
		$user->setName('admin');
		$user->setPassword('1234');
		$user->setMail('admin@nomail.com');
		$user->setPsn('admin');
		$user->setRoles('ROLE_ADMIN');
		$manager->persist($user);
		for ($i=0;$i<30;$i++){
			$user= new Entity\User();
			$user->setName('user'.$i);
			$user->setPassword('1234');
			$user->setMail('user'.$i.'@nomail.com');
			$user->setPsn('psnUser'.$i);
			$user->setRoles('ROLE_USER');
			//$user->addInscripcion();
			$manager->persist($user);
		}
		$manager->flush();
	}

}