<?php

namespace MGTRM\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MGTRM\AdminBundle\Entity\User;
use MGTRM\AdminBundle\Form\UserType;

class RegisterUserController extends Controller
{
    public function indexAction()
    {
		/*if($this->get('security.context')->isGranted('ROLE_USER')){
			return $this->redirect($this->generateUrl('jumbotron'));
		}*/
		$valid=true;
		$entity = new User();
		$form   = $this->createCreateForm($entity);
		$request=$this->getRequest();
		if($request->getMethod()=="POST"){
			$form->handleRequest($request);
			if($form->isValid()){
				$em = $this->getDoctrine()->getManager();
				$em->persist($entity);
				$em->flush();

				return $this->redirect($this->generateUrl('login'));
			}
			$valid=false;
		}
        return $this->render('FrontendBundle:RegisterUser:index.html.twig',
			array(
				'form'=>$form->createView(),
				'valid'=>$valid
		));
    }

	private function createCreateForm(User $entity)
	{
		$form = $this->createForm(new UserType(), $entity, array(
			'action' => $this->generateUrl('register_user'),
			'method' => 'POST',
		));

		$form->add('submit', 'submit', array('label' => 'Registrar usuario','attr'=>array('class'=>'btn btn-primary')));

		return $form;
	}
}
