<?php

namespace MGTRM\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
		$em=$this->getDoctrine()->getManager();
		$entity=$em->getRepository('AdminBundle:Torneo');
		$activeTournaments=$entity->getActiveTournaments();
		$waitingTournaments=$entity->getPendingTournaments();
		$closedTournaments=$entity->getClosedTournaments();
        return $this->render('FrontendBundle:Default:index.html.twig',array(
			'tournaments'=>array(
				'active'=>$activeTournaments,
				'waiting'=>$waitingTournaments,
				'closed'=>$closedTournaments
			)
		));
    }

	public function jumbotronAction(){
		return $this->render('FrontendBundle:Default:jumbotron.html.twig');
	}
}
