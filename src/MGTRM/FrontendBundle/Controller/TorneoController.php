<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 13/03/14
 * Time: 18:31
 */

namespace MGTRM\FrontendBundle\Controller;

use MGTRM\AdminBundle\Form\TorneoType;
use MGTRM\FrontendBundle\Form\TorneoUploadType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MGTRM\AdminBundle\Entity;

class TorneoController extends Controller{


	public function indexAction(){

	}

	public function infoAction($id){
		$em=$this->getDoctrine()->getManager();
		$entity=$em->getRepository('AdminBundle:Torneo');
		$torneo=$entity->find($id);
		if(!$torneo){
			return $this->redirect('torneos_list');
		}
		$expired=$this->_isDateTimeExpired($torneo->getTDatetime());

		//Creamos el formulario de subida de imagen
		$tType=new TorneoUploadType();
		$form = $this->createForm($tType, null, array(
			'action' => $this->generateUrl('torneo_uploadPhoto',array('id' => $id)),
			'method' => 'PUT',
		));

		$vars=array(
			'torneo'=>$torneo,
			'expired'=>$expired,
			'inscribed'=>$torneo->getInscritos()->contains($this->getUser()),
			'photoForm'=>$form->createView());

		if($torneo->getStatus()==Entity\Torneo::STATUS_APPROVED){
			$results=$em->getRepository('AdminBundle:Results')->getOrderedResults($torneo->getId());
			foreach($results as $elem){
				$drivers[]=$elem->getUser()->getName();
			}
			$vars['results']=$drivers;
		}

		return $this->render('FrontendBundle:Torneo:index.html.twig',$vars);
	}

	public function inscribeAction($id){
		$em=$this->getDoctrine()->getManager();
		$torneo=$em->getRepository('AdminBundle:Torneo')->find($id);
		if($this->_isDateTimeExpired($torneo->getTDatetime())){
			return $this->redirect($this->generateUrl('torneo_info',array('id'=>$id)));
		}
		$currentUser=$this->getUser();
		//Comprobamos si el usuario está ya inscrito

		$inscritos=$torneo->getInscritos();
		if(!$inscritos->contains($currentUser))
		{
			$currentUser->addInscripcion($torneo);
			$em->flush();

		}else{
			$currentUser->removeInscripcion($torneo);
			$em->flush();
		}
		return $this->redirect($this->generateUrl('torneo_info',array('id'=>$id)));

	}

	public function uploadAction($id){
		$request=$this->getRequest();
		$em=$this->getDoctrine()->getManager();
		$entity=$em->getRepository('AdminBundle:Torneo')->find($id);
		if(!$entity){
			$this->redirect('torneos_list');
		}
		$form = $this->createForm(new TorneoUploadType(), null, array(
			'action' => $this->generateUrl('admin_torneo_update', array('id' => $entity->getId())),
			'method' => 'PUT',
		));
		$form->handleRequest($this->getRequest());

		if($form->isValid()){
			$foto=$form['photo']->getData();
			$nombre= \uniqid().'.'.$foto->getClientOriginalExtension();
			$mime=$foto->getMimeType();
			$foto->move(__DIR__.'/../../../../web/upload',$nombre);
			//$entity->setResultImage(__DIR__.'/../../../../web/upload/'.$nombre);
			$entity->setResultImage($request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath() .'/upload/'.$nombre);
			$entity->setImageMime($mime);
			$entity->setUploaderId($this->getUser()->getId());
			$entity->setStatus(Entity\Torneo::STATUS_WAITING);
			$em->flush();

		}
		return $this->redirect($this->generateUrl('torneo_info',array('id'=>$id)));

	}

	private function _isDateTimeExpired(\DateTime $date){
		$current= new \DateTime();
		$diff=$current->diff($date);
		if($diff->invert==1){
			return true;
		}else{
			return false;
		}
	}



} 