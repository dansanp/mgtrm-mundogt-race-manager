<?php

namespace MGTRM\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Image;


class TorneoUploadType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder,array $options){
		$builder
			->add('photo','file',array(
			'label'=>'Foto de los resultados:',
			'constraints'=>array(
				new Image()
			)
		))
		->add('submit','submit',array(
				'label'=>'Subir foto',
				'attr'=>array(
					'class'=>'btn btn-primary',
					'style'=>'margin-top:15px'
				)
			));
	}

	public function getName(){
		return 'TorneoUpload';
	}

	/*public function setDefaultOptions(OptionsResolverInterface $resolver){
		$resolver->setDefaults(array(
			'data_class'=>'MGTRM\FrontendBundle\Entity\UploadedPhoto'
		));
	}*/
}