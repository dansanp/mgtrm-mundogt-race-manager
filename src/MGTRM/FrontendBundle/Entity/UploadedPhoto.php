<?php
/**
 * Created by PhpStorm.
 * User: dansanp
 * Date: 15/03/14
 * Time: 17:08
 */

namespace MGTRM\FrontendBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;


class UploadedPhoto {
	/**
	 * @Assert\Image
	 */
	private $foto;

	public function setFoto(UploadedFile $foto = null){
		$this->foto=$foto;

	}

	public function getFoto(){
		return $this->foto;
	}

} 