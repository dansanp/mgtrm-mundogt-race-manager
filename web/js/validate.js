$(function(){
    $('#validate').click(function(event){
        event.preventDefault();
        var emptySelects=false;
        //Comprobamos si alguno no esta seleccionado
        $("form select").each(function(index){
            if($(this).val()==''){
                emptySelects=true;
            }
        });
        if(emptySelects){
            $("#emptyError").slideDown();
            return;
        }
        else{
            $("#emptyError").slideUp();
        }

        //Comprobamos que no hay campos repetidos
        var i=0;
        var j=0;
        var length=$("form select").length;
        var repeated=false;
        for (i;i<length;i++){
            var value=$("form select").eq(i).val();
            for(j;j<length;j++){
                if(i!=j){
                    if($("form select").eq(j).val()==value){
                        repeated=true;
                    }
                }
            }
        }

        if(repeated){
            $("#repeatedError").slideDown();
        }
        else{
            $("#repeatedError").slideUp();
            $('form').submit();
        }
    });
});