
var deleteRoot='http://symfony.dev/app_dev.php/admin/borrar/';

$(function(){
    $("a[class='btn btn-danger']").each(function(index){
        $(this).click(function(event){
            event.preventDefault();
            var id=$(this).data("id");
            deleteUrl=deleteRoot+id;
            $('#modal_text').html('¿Desea eliminar el torneo "'+$(this).data("name")+'"?');
            $('#modal_ok').attr('href',deleteUrl);
            $('#deleteModal').modal('show');
        });
    });
});